
public class Jellyfish {
	
	//System.out.println("");
	
	//fields
	public String colour;
	public String shape;
	public boolean swimmer;
	
	
	public void identifyJellyfish(){
		
		System.out.println("Identifying your jellyfish...");
		
		if (colour.equals("transparent") && 
			shape.equals("saucer") && !swimmer) {
				
			System.out.println("You've found a Moon Jellyfish!");
				
		} else if (colour.equals("blue") && 
			shape.equals("box") && swimmer) {
			
			System.out.println("Be careful, that's a Box Jellyfish!");
		
		} else {
			
			System.out.println("Jellyfish Indentity Unknown.");
		}
	}
	
	public void checkVenom(){
		
		if (!swimmer) {
			System.out.println("It has a bit of venom, bit it'll only irritate your skin.");
			
		} else {
			System.out.println("Its sting is super lethal!");
		
		}
	}
}