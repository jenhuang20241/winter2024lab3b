import java.util.Scanner;


public class VirtualPetApp {
	
	public static void main (String [] args){
		Scanner reader = new Scanner(System.in);
		
		//System.out.println("");
		
		Jellyfish[] bloom = new Jellyfish[5];
		
		System.out.println("Jellyfish Identifier");
		System.out.println("Please answer the following questions");
		
		
		for (int i=0; i<bloom.length; i++){
			
			bloom[i] = new Jellyfish();
			
			
			// colour question
			System.out.println("What colour is this jellyfish?");
			
			bloom[i].colour = reader.nextLine();
			
			// shape
			System.out.println("What shape is it?");
			bloom[i].shape = reader.nextLine();
			
			
			//swimmer question
			System.out.println("Can it swim on its own? Y/N");
			String answer = reader.next();
			
			if(answer.equals("y") || answer.equals("Y")){
				bloom[i].swimmer = true;
			
			} else {
				bloom[i].swimmer = false;
			}
			
			
		}
			System.out.println(bloom[bloom.length-1].colour);
			System.out.println(bloom[bloom.length-1].shape);
			System.out.println(bloom[bloom.length-1].swimmer);
	
		
			bloom[0].identifyJellyfish();
			bloom[0].checkVenom();
	}


}